const hourHand = document.getElementById("hand_hour");
const minuteHand = document.getElementById("hand_min");
const secondHand = document.getElementById("hand_sec");


function setClock() {
    const currentTime = new Date();
    const secondsRatio = currentTime.getSeconds() / 60;
    const minutesRatio = (currentTime.getMinutes() +  secondsRatio) / 60;
    const hourRation = (currentTime.getHours() + minutesRatio) / 12;

    setRotation(secondHand, secondsRatio);
    setRotation(minuteHand, minutesRatio);
    setRotation(hourHand, hourRation);
}


function setRotation(element, rotationRatio) {
    element.style.setProperty('--rotation', rotationRatio * 360);
}

setClock();
let intervalCb = setInterval(setClock, 500);