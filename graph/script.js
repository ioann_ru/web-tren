const area = document.getElementById('area');
function isInt(x) {
    return typeof(x) == 'number' && parseInt(x) == x;
}
function assert(cond, msg) {
    if (!cond) {
        alert(msg);
    }
}
function drawPerfect(node, n) {
    // if (typeof(n) != 'number' or n 
    parseFloat(n);
    assert(isInt(n), "non integer number of corners <" + n + ">");
    console.log("Ok")

    
}

// TODO proper location on area resize!

// drawPerfect(area, 1.2)

function createCircleInArea() {
    var c = document.createElement('div');
    area.appendChild(c);
    c.classList.add('circle');
    return c;
}

function createDragableCircleInArea() {
    var c = createCircleInArea();
    c.classList.add('dragable');
    return c;
}

var circles = [];

function px2percent(x, y, areaX, areaY) {
    return [(x * 100) / areaX + '%', (y * 100) /areaY + '%'];
}

function drawLine(startX, startY, endX, endY) {
    var [dx, dy] = [endX - startX, endY - startY];
    const len = Math.sqrt(dx*dx + dy*dy);
    var line = document.createElement('div');
    line.classList.add('line');
    area.appendChild(line);
    line.style['left'] = startX + 'px';
    line.style['top'] = startY + 'px';
    line.style.width = len + 'px';
    
    return line;
}

function centerOfNode(node) {
    return [node.offsetLeft - node.offsetWidth/2, node.offsetTop - node.offsetHeight/2];
}
function coordsOfNode(node) {
    return [node.offsetLeft, node.offsetTop];
}

var line;

area.onclick = function(e) {
    console.log(e);
    c = createDragableCircleInArea();
    // let [x, y] = centerOfNode(c);
    [c.style['left'], c.style['top']] = //[e.clientX, e.clientY].map(x => x + "px");
     px2percent(e.clientX , e.clientY, area.offsetWidth, area.offsetHeight);
    // circles.push(c);

    // console.log(c); 
    let n = createDragableCircleInArea();
    n.classList.add('dot');

    [n.style['left'], n.style['top']] = px2percent(...coordsOfNode(c), area.offsetWidth, area.offsetHeight);
    console.log(n);
    console.log(c);
    // for (let i = 1; i != circles.length;  ++i) {
    //     // for (let j = 0; j != i; ++j) {
    //     //     const [startX, startY] = centerOfNode(circles[i]);
    //     //     const [endX, endY] = centerOfNode(circles[j]);
    //     //     line = drawLine(startX, startY, endX, endY);
    //     // }

    // }
};



function VerticalLiner(container, n) {
    let vline = document.createElement('div');
    vline.classList.add('vline')
    for (let i = 0; i < n - 1; ++i) {
        container.appendChild(vline.cloneNode())
    }
    container.appendChild(vline);
}